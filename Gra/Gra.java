import java.util.Random;
import java.util.Scanner;

class Gra {

    public static void main(String[] args) 
	{
		try
		{
			if(args.length == 0) throw new Exception("Brak przedzialu");
			if(args.length >1) throw new Exception("Nieodpowiednie argumenty wejsciowe");
		
			boolean system,ponownaGra=true;
			int N = Integer.parseInt(args[0]);
			SystemGry gra = new SystemGry(N);
			
			do{
			system=true;
			
				do{
					gra.DodajProbe();
					System.out.println("Podaj liczbe: ");
					Scanner klawiatura = new Scanner(System.in);
					int liczba_wczytana = klawiatura.nextInt();
					
					if(gra.Compare(liczba_wczytana)==1) 
					{
						System.out.print("\033[H\033[2J");
						System.out.println("Liczba trafiona! -> "+liczba_wczytana);	
						System.out.println("Ilosc prob: "+gra.getIloscProb());
						system=false;
					}
					else if(gra.Compare(liczba_wczytana)==2) 
					{
						System.out.print("\033[H\033[2J");
						System.out.println("Twoja liczba ("+liczba_wczytana+") wieksza");	
					}
					else
					{
						System.out.print("\033[H\033[2J");
						System.out.println("Twoja liczba ("+liczba_wczytana+")  mniejsza");	
					}
					
				}while(system);
		
			
			System.out.println("Czy chcesz zagrać ponownie? (tak/nie)");	
			Scanner klawiatura2 = new Scanner(System.in);
			String ponowne_granie = klawiatura2.next();
			gra.NowaGra();
			
			if(ponowne_granie.compareTo("nie")==0) ponownaGra=false; 
			
			}while(ponownaGra);
		
	
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
}

class SystemGry {
	
private int WylosowanaLiczba;
private int IloscProb;
private int Przedzial;

	public SystemGry(int n)
	{
		this.WylosowanaLiczba=Losowanie(n);
		this.IloscProb=0;
		this.Przedzial=n;
	}
	
	private int Losowanie(int n)
	{
		Random r = new Random();
		return r.nextInt(n+1);
		
	}
	
	public int Compare(int liczba)
	{
		
	
		if(liczba==this.WylosowanaLiczba)
		{
				return 1;
		}
		else if(liczba>this.WylosowanaLiczba)
		{
				return 2;
		}
		else // (liczba<this.WylosowanaLiczba)
		{
			return 3;
		}
	}
	
	public void NowaGra()
	{
		this.WylosowanaLiczba=Losowanie(this.Przedzial);
		this.IloscProb=0;
	}
	public void DodajProbe()
	{
		this.IloscProb++;	
	}
	public int getIloscProb()
	{
		return this.IloscProb;
	}
}


