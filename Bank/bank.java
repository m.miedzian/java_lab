import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.*;

class Bank {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

	DataBase Base = new DataBase();
	
	
// 	for(int i=0;i<10;i++)
// 	Base.addClient("Janek", "Kowalski", 1010, "Lodz");
// 	
// 	
// 	Base.saveDataBase("Base.dat");
	
	
Scanner klawiatura = new Scanner(System.in);
int liczba_wczytana = klawiatura.nextInt();

	if(liczba_wczytana==1) 
	{
	for(int i=0;i<10;i++)
	Base.addClient("Janek", "Kowalski", "1010", "Lodz");

	Base.addClient("Marek", "Zdun", "12345", "Lodz");
	Base.saveDataBase("Base.dat");
	System.out.println("Baza zapisana");
	}
	else 
	{
		
	Base.loadDataBase("Base.dat");

	
	
	

	Base.getClientByID(SearchEngine.searchIdByAccountNumber(Base,105)).getAccount().paymentOnAccount(500);
	

	for(int i=0;i<Base.numberOfClients;i++)
	System.out.println(Base.getClient(i).getID() +  "  Saldo:" + Base.getClient(i).getAccount().getAccountBalance() + "  Nr konta:" + Base.getClient(i).getAccount().getAccountNumber());	
	Base.getClientByID(SearchEngine.searchIdByAccountNumber(Base,105)).getAccount().transfer(Base,107,100);
	System.out.println("  -- PO PRZELEWIE -- ");
	for(int i=0;i<Base.numberOfClients;i++)
	System.out.println(Base.getClient(i).getID() +  "  Saldo:" + Base.getClient(i).getAccount().getAccountBalance() + "  Nr konta:" + Base.getClient(i).getAccount().getAccountNumber());	
	
	for(int a:SearchEngine.searchByName(Base, "Baska")) System.out.println(a);
	
	Base.saveDataBase("Base.dat");
		
	}
	
	


	
	}
	
}


class DataBase {
	
	private Client[] clientsArray;
	public int numberOfClients;


	public DataBase() {
		clientsArray = new Client[10];
		
		numberOfClients = 0;
	}
	
	public void addClient(String name, String surname, String pesel, String address) {
		
		checkSizeOfArray();
		clientsArray[numberOfClients] = new Client(name, surname, pesel, address,  this);
		numberOfClients++;
	}
	
	
	public int getNumberOfClients() {
		return this.numberOfClients;
	}
	
	private void checkSizeOfArray() {
		if(numberOfClients + 1 == clientsArray.length) {
			Client[] tempArray = new Client[clientsArray.length];
			System.arraycopy(clientsArray,0,tempArray,0,clientsArray.length);
			clientsArray = new Client[tempArray.length+10];
			System.arraycopy(tempArray,0,clientsArray,0,tempArray.length);	
		}
		
	}
	
	public void saveDataBase(String nameOfFile) throws IOException {
			
		ObjectOutputStream ob = null;
		
		try {
			ob=new ObjectOutputStream(new FileOutputStream(nameOfFile));
			
			for(int i = 0; i < numberOfClients; i++) {
				ob.writeObject(clientsArray[i]);
			}
            
            ob.flush();
        }
        finally {
			if(ob != null)
			ob.close();
        }
		
		
	}
	
	public void loadDataBase(String nameOfFile) throws IOException,ClassNotFoundException {
		
		ObjectInputStream ob2 = null;
        Client clientObject = null;
        try{
            ob2 = new ObjectInputStream(new FileInputStream(nameOfFile));
            
			int i = 0;
            while(true){
				checkSizeOfArray();
				clientsArray[i]= new Client();
				clientObject = (Client)ob2.readObject();
				clientsArray[i] = clientObject;
				i++;
				numberOfClients++;
            }
		}
		catch (EOFException ex) {
            
        }	
        finally{
			
			if(ob2 != null)
			ob2.close();
        }
		
	}
	
	public Client getClient(int index) {
			return this.clientsArray[index];
	}
	public Client getClientByID(int index) {
			return this.clientsArray[index-1];
	}
	
}




class Client implements Serializable {
	
	private String 	name;
	private String surname;
	private int id;
	private String pesel;
	private String address;
	private Account accountObject;
	
	public Client() {
		
		
	}
	
	
	public Client(String name, String surname, String pesel, String address,  DataBase object) {
	
		this.name=name;
		this.surname=surname;
		this.pesel=pesel;
		this.address=address;
		this.id=object.getNumberOfClients() + 1;
		this.accountObject = new Account(this);
	}
	
	
	
	public String getName() {
			return this.name;
	}
	public String getSurname() {
			return this.surname;
	}
	public int getID() {
			return this.id;
	}
	public Account getAccount() {
		return this.accountObject;
	}
	public String getPesel() {
			return this.pesel;
	}
	public String getAddress() {
			return this.address;
	}
}

class Account implements Serializable {

private int accountBalance;
private int accountNumber;

	public Account(Client object) {
		this.accountBalance = 0;
		this.accountNumber = object.getID() + 100;
		
	}
	
	public int getAccountNumber () {
		return this.accountNumber;
	}
	public void paymentOnAccount (int money) {
		this.accountBalance = this.accountBalance + money;
	}
	public boolean paymentFromAccount (int money) {
		if(this.accountBalance < money) {
				return false;
		}
		else {
			this.accountBalance = this.accountBalance - money;
			return true;
			
		}
			
		}
	public boolean transfer (DataBase currentBase, int accountNumber, int money) {
		if(this.accountBalance < money) {
				return false;
		}
		else {
			
		this.accountBalance = this.accountBalance - money;
		System.out.println(this.getAccountNumber());
		
		currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().accountBalance = 
		currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().accountBalance + money;
		
		System.out.println(currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().getAccountNumber());
		return true;
		}
		
	}
	public int getAccountBalance () {
			return this.accountBalance;
	}
}

class SearchEngine {
	
	public static int searchIdByAccountNumber(DataBase currentBase,int searchNumber) {
		
		for(int i=0; i<currentBase.getNumberOfClients(); i++) {
				if(searchNumber == currentBase.getClient(i).getAccount().getAccountNumber()) {
					return currentBase.getClient(i).getID();
						
				}
			
		}
		return -1; // Zwraca -1 jeżeli nie znajdzie konta w bazie
	}
	public static int searchIdByPesel(DataBase currentBase,String searchPesel) {
			for(int i=0; i<currentBase.getNumberOfClients(); i++) {
				if(currentBase.getClient(i).getPesel().compareTo(searchPesel) == 0) {
					return currentBase.getClient(i).getID();
				}
			}
			return -1;
	}
	public static int[] searchByName (DataBase currentBase, String name) {
		
		int numberOfRecords = 0;
		for(int i=0; i < currentBase.getNumberOfClients(); i++) {
			if(currentBase.getClient(i).getName().compareTo(name) == 0) {
				numberOfRecords++;
			}
		}
		int[] tempArray = new int[numberOfRecords];
			
		int temp = 0;	
		for(int i=0; i < currentBase.getNumberOfClients(); i++) {
			if(currentBase.getClient(i).getName().compareTo(name) == 0) {
				tempArray[temp]=currentBase.getClient(i).getID();
				temp++;
			}
		}

		return tempArray;
	}
	
	public static int[] searchBySurname (DataBase currentBase, String Surname) {
		
		int numberOfRecords = 0;
		for(int i=0; i < currentBase.getNumberOfClients(); i++) {
			if(currentBase.getClient(i).getSurname().compareTo(Surname) == 0) {
				numberOfRecords++;
			}
		}
		int[] tempArray = new int[numberOfRecords];
			
		int temp = 0;	
		for(int i=0; i < currentBase.getNumberOfClients(); i++) {
			if(currentBase.getClient(i).getSurname().compareTo(Surname) == 0) {
				tempArray[temp]=currentBase.getClient(i).getID();
				temp++;
			}
		}

		return tempArray;
	}
	
	public static int[] searchByAddress (DataBase currentBase, String Address) {
		
		int numberOfRecords = 0;
		for(int i=0; i < currentBase.getNumberOfClients(); i++) {
			if(currentBase.getClient(i).getAddress().compareTo(Address) == 0) {
				numberOfRecords++;
			}
		}
		int[] tempArray = new int[numberOfRecords];
			
		int temp = 0;	
		for(int i=0; i < currentBase.getNumberOfClients(); i++) {
			if(currentBase.getClient(i).getAddress().compareTo(Address) == 0) {
				tempArray[temp]=currentBase.getClient(i).getID();
				temp++;
			}
		}

		return tempArray;
	}
}