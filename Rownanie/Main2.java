class Main2 {

	static void Display(double a, double b, double c)
	{
		RownanieKwadratowe rownanie = new RownanieKwadratowe(a,b,c);
				
		if(rownanie.Rozwiazanie()==1) 
		{
			System.out.println("Rownanie posiada jedno rozwiazanie: "+rownanie.getX1());
		}	
		else if(rownanie.Rozwiazanie()==2)
		{
			System.out.println("Rownanie posiada dwa rozwiazania x1: "+rownanie.getX1()+" x2: "+rownanie.getX2());
		}
		else
		{
			
			System.out.println("Brak rozwiazan rownania");
		}
	}
	
	
    public static void main(String[] args) 
	{
		try
		{
			System.out.print("\033[H\033[2J"); // Clear screen
			if(args.length <3) throw new Exception("Zbyt mala ilosc argumentow wejsciowych");
			if(args.length >3) throw new Exception("Zbyt duza ilosc argumentow wejsciowych");
			

			double a=Double.parseDouble(args[0]); // parseDouble domyślnie rzuca wyjątkami, więc tutaj nie uwzględniam wyjątków - wyczytane w dokumentacji klasy
			double b=Double.parseDouble(args[1]);
			double c=Double.parseDouble(args[2]);
			
			
			if(a==0) 
			{
				System.out.println("To nie jest rownanie kwadratowe");
			}
			else
			{
				Display(a,b,c);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}	
    }

    
   
}


class RownanieKwadratowe {
	private double a;
	private double b;
	private double c;
	private double delta;
	private double x1;
	private double x2;
	
	public RownanieKwadratowe(double a,double b,double c)
	{		
		this.a=a;
		this.b=b;
		this.c=c;
		delta=delta(this.a,this.b,this.c);
	}
	
	private double delta(double a,double b,double c)
	{
		return (b*b)-(4*(a*c));
	}
	
	public int Rozwiazanie()
	{	
		if(this.delta==0) 
		{
			this.x1=(-b)/(2*a);
			this.x2=this.x1;
			return 1;
		}
		else if(this.delta>0)
		{
			this.x1=((-b)-Math.sqrt(delta))/(2*a);
			this.x2=((-b)+Math.sqrt(delta))/(2*a);
			return 2;
		}
		else
		{
			return 3;
		}
	}
		
	public double getX1()
	{
		return this.x1;
	}
		
	public double getX2() 
	{
		return this.x2;
	}
}